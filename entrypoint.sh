#! /bin/sh

# Start opentracker in the background.
/opentracker -f /opentracker.conf &

# Get opentracker's PID.
OPENTRACKER_PID=$(pidof opentracker)

# Send SIGHUP to opentracker every time the whitelist file changes.
find whitelist | entr -n kill -s SIGHUP "$OPENTRACKER_PID" &

wait
