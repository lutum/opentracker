# opentracker

This is a container with [opentracker](https://erdgeist.org/arts/software/opentracker/) built from source. Since it's meant to be used with [mycelium](https://gitlab.com/lutum/mycelium), some feature flags and configuration values have been changed from their defaults, so if you plan to use it, check the [Dockerfile](https://gitlab.com/lutum/opentracker/-/blob/main/Dockerfile) first. There's also [entr](https://eradman.com/entrproject/) included, which notifies opentracker when the whitelist file changes.
