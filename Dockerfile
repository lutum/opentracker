FROM alpine:3.15 AS build

# Install build dependencies.
RUN apk add --no-cache \
    cvs \
    g++ \
    gcc \
    git \
    make \
    zlib-static

# Build libowfat.
RUN cvs -d :pserver:cvs@cvs.fefe.de:/cvs -z9 co libowfat \
 && make --directory libowfat

# Build opentracker.
# https://erdgeist.org/gitweb/opentracker/tree/Makefile
ARG MAKEFILE_CHANGES="/FEATURES+=-DWANT_ACCESSLIST_WHITE/{s/#//}; \
                      /FEATURES+=-DWANT_RESTRICT_STATS/{s/#//}; \
                      /FEATURES+=-DWANT_FULLSCRAPE/{s/^/#/}"
WORKDIR /opentracker
RUN git clone git://erdgeist.org/opentracker . \
 && sed --in-place "$MAKEFILE_CHANGES" Makefile \
 && LDFLAGS=-static make

# Prepare opentracker's config file.
# https://erdgeist.org/gitweb/opentracker/tree/opentracker.conf.sample
ARG CONFIG_CHANGES="s/# access.whitelist \/path\/to\/whitelist/access.whitelist \/whitelist/; \
                    s/# access.stats 192.168.0.23/access.stats 127.0.0.1/"
RUN sed --in-place "$CONFIG_CHANGES" opentracker.conf.sample

# Remove build dependencies.
FROM alpine:3.15
COPY --from=build /opentracker/opentracker /opentracker
COPY --from=build /opentracker/opentracker.conf.sample /opentracker.conf

RUN apk add --no-cache entr

COPY ./entrypoint.sh /entrypoint.sh
RUN ["chmod", "+x", "/entrypoint.sh"]

ENTRYPOINT ["/entrypoint.sh"]
